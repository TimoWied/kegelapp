<?php
namespace App\Tests\Unit\Form;

use App\Form\TeamMemberType;
use Symfony\Component\Form\Test\TypeTestCase as SymfonyTypeTestCase;

class TeamMemberTypeTest extends SymfonyTypeTestCase
{

    /**
     * @param $formData
     * @param $expectedFormData
     * @dataProvider getValidData
     */
    public function testValidFormData($formData, $expectedFormData)
    {
        $form = $this->factory->create(TeamMemberType::class);
        $form->submit($formData);

        $this->assertTrue($form->isValid());
        $this->assertEquals($expectedFormData, $form->getData());
    }

    public function getValidData()
    {
        return [
            'complete' => [
                [
                    'full_name' => 'Max Mustermann',
                    'status' => 'member',
                ], [
                    'full_name' => 'Max Mustermann',
                    'status' => 'member',
                ]
            ]
        ];
    }

    /**
     * @param $formData
     * @dataProvider getInvalidData
     */
    public function testSubmitInvalidData($formData)
    {
        $form = $this->factory->create(TeamMemberType::class);
        $form->submit($formData);

        $this->assertFalse($form->isValid());
    }

    public function getInvalidData()
    {
        return [
            'no fullname' => [
                [
                    'full_name' => null,
                    'status' => 'member',
                ],
            ],
            'no status' => [
                [
                    'full_name' => 'Max Mustermann',
                    'status' => null,
                ],
            ],
            'wrong status' => [
                [
                    'full_name' => 'Max Mustermann',
                    'status' => 'foobar',
                ],
            ],
        ];
    }

}
