<?php
namespace App\Exception;

class EntityNotFoundException extends \Exception
{

    public static function createFromEntityClassAndEntityId($entityName, $id)
    {
        return new self(sprintf('%s with id %s not found.', $entityName, $id));
    }
}
