<?php
namespace App\Controller;

use App\Entity\TeamMember;
use App\Exception\EntityNotFoundException;
use App\Form\TeamMemberType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class TeamMemberController extends Controller
{

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws EntityNotFoundException
     */
    public function createTeamMember(Request $request)
    {
        return $this->createAndHandleForm($request);
    }

    /**
     * @param $teamMemberId
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws EntityNotFoundException
     */
    public function updateTeamMember($teamMemberId, Request $request)
    {
        return $this->createAndHandleForm($request, $teamMemberId);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getTeamMember()
    {
        $teamMembers = $this->getDoctrine()->getRepository(TeamMember::class)->findAll();

        return $this->render('teammember/teammemberlist.html.twig', [
            'teamMembers' => $teamMembers,
        ]);
    }

    /**
     * @param Request $request
     * @param null $teamMemberId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws EntityNotFoundException
     */
    private function createAndHandleForm(Request $request, $teamMemberId = null)
    {
        $teamMember = new TeamMember();
        if ($teamMemberId) {
            $teamMember = $this->getDoctrine()->getRepository(TeamMember::class)->find($teamMemberId);
            if (!$teamMember) {
                throw EntityNotFoundException::createFromEntityClassAndEntityId(TeamMember::class, $teamMemberId);
            }
        }
        $form = $this->createForm(TeamMemberType::class, $teamMember);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            if (!$teamMemberId) {
                $entityManager->persist($teamMember);
            }
            $entityManager->flush();

            $this->addFlash(
                'notice',
                'Erfolgreich gespeichert!'
            );

            return $this->redirectToRoute('app_ninepins_teammember_get');
        }

        return $this->render('teammember/teammembercreate.html.twig', [
            'form' => $form->createView(),
            'teamMember' => $teamMember
        ]);
    }

    /**
     * @param $teamMemberId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws EntityNotFoundException
     */
    public function deleteTeamMember ($teamMemberId)
    {
        $teamMember = $this->getDoctrine()->getRepository(TeamMember::class)->find($teamMemberId);
        if (!$teamMember) {
            throw EntityNotFoundException::createFromEntityClassAndEntityId(TeamMember::class, $teamMemberId);
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($teamMember);
        $entityManager->flush();

        $this->addFlash(
            'notice',
            'Mitglied gelöscht!'
        );

        return $this->redirectToRoute('app_ninepins_teammember_get');
    }
}