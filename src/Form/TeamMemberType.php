<?php
namespace App\Form;

use App\Entity\TeamMember;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\NotBlank;

class TeamMemberType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('full_name', TextType::class,
                [
                    'required' => true,
                    'constraints' => [
                        new NotBlank(),
                    ]
                ]
            )
            ->add('status', ChoiceType::class,
                [
                    'choices' => ['Mitglied' => TeamMember::STATUS_MEMBER, 'Gast' => TeamMember::STATUS_GUEST],
                    'required' => true,
                    'constraints' => [
                        new NotBlank(),
                        new Choice([
                            TeamMember::STATUS_MEMBER,
                            TeamMember::STATUS_GUEST,
                        ]),
                    ],
                ]
            )
            ->add('save', SubmitType::class,
                [
                    'label' => 'Speichern',
                    'attr' => ['class' => 'btn btn-dark btn-sm active pull-right'],
                ]
            )
        ;
    }
}